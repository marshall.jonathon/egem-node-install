#!/bin/sh
LIST_OF_APPS="build-essential git screen fail2ban curl zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev nginx nodejs make unzip pkg-config tcl -y"

apt-get update  # To get the latest package lists
apt-get upgrade -y
apt-get install -y $LIST_OF_APPS

# Clean previous go Install and download
rm -r go
rm -r /usr/local/go
rm -r go1.18.8.linux-amd64.tar.gz
# Download & Install Go-lang

# Download
# cd ..
wget https://golang.org/dl/go1.18.8.linux-amd64.tar.gz
# Unpack & Move
tar -C /usr/local -xzf go1.18.8.linux-amd64.tar.gz

mkdir go
sleep 1
echo "Updated! follow next step of install."
