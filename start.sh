#!/bin/bash

NODE_PARAMS=''

# Make sure node is closed.
screen -S egemnode -X stuff $'\003'
echo "---Sending Crtl C: egemnode"
screen -S egemnode -X stuff 'exit \n'
echo "---Exiting screen: egemnode"
sleep 1

# Open node screen
screen -dmS egemnode
echo "---Starting new screen: egemnode"
sleep 1

# Run node in screen
screen -S egemnode -X stuff './go-egem/build/bin/egem $NODE_PARAMS \n'
echo "---Sending startup to screen for: egemnode"
sleep 1
echo "---EGEM node is now running on screen: egemnode"
exit
